#export DISPLAY=:3
#
#	AUTHOR - Raman Sailopal
#
#	Main build script for Jenkins. Allows automatic build and setup of a#	    new mysql/php test environment.
#
#
#	These next four variables need to be set up according to your own lo#	    local configurations.
#
#
masterdir="Intranet"
masterroot="/srv/www/htdocs/"
cpyroot="/srv/www/htdocs/"
mysqlroot="/var/lib/mysql/"
#
#
# Phase one of the auto build - copy core code to a new test directory prepended with the build number i.e. Intranet75
#
cp -R $masterroot$masterdir $masterroot$masterdir$(echo "$(cat nextBuildNumber) - 1" | bc)
#
# Phase two is to change all references to the original databases in the code and replace it with the newly built databases. List of databases are attained from the dbases.txt file. So this applies for intranetref and db_intranet in this case
#
for var in $(cat dbases.txt)
do
	ram="find $masterroot"$masterdir$(echo $(cat nextBuildNumber) - 1 | bc)" -name '*.php' -exec sed -in 's/$var/$var_"$(echo $(cat 'nextBuildNumber') - 1 | bc)"/' '{}' \;"
	echo $ram | sh
done
#
# Final phase is to copy the databases (again intranetref and db_intranet databases in this case) from the original to new. Easy as the database is set up with MyISAM database schema
#
for var in $(cat dbases.txt)
do
	cp -R --preserve $mysqlroot$var $mysqlroot$var$(echo $(cat nextBuildNumber) - 1 | bc)
	chmod -R 775 $mysqlroot$var$(echo $(cat nextBuildNumber) - 1 | bc)
done
